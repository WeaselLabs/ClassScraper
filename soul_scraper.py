#!/usr/bin/python
import os
import json
import time
from bs4 import BeautifulSoup
from splinter import Browser
import re
from spinclass import SpinClass
import csv
import ast
import random
import datetime
import copy
import argparse

BASE_FILEPATH = os.path.realpath(os.path.dirname(__file__))


def return_urls(browser, url_list, config, state):
    '''
    take in a url (or list of urls) and return a larger dictionary of urls based in links in the
    config file
    '''
    new_urls = {}
    for tag, term in config[state]['tags'].items():
        links = set()
        for url in url_list:
            link_elements = select_blocks(browser, url, term)
            for link in link_elements:
                # match the element that has the reserve a bike url
                link_block = link.find_all('a', href=re.compile(config[state]['href']))
                if link_block is None: continue
                for link_sublock in link_block:
                    links.update(["{}{}".format(config['base_url'], link_sublock['href'])])
        new_urls[tag] = links
    return new_urls


def select_blocks(browser, url, tag):
    ''' take url and class term and return all block dicts with that term on that page'''
    browse(browser, url)
    site_html = browser.html
    soup = BeautifulSoup(site_html, 'html.parser')
    return soup.find_all(tag['blocktype'], class_=tag['class'])


def find_block(browser, url, class_tag):
    '''
    given a class name, return the first block containing that tag
    '''
    browse(browser, url)
    site_html = browser.html
    soup = BeautifulSoup(site_html, 'html.parser')
    return soup.find(class_=class_tag)


def grab_data(block, data):
    ''' takes in a soup object block and returns the data from the dictionary in that block'''
    data_dict = {}
    for key, field in data.items():
        if key == "header" or key == "timeformat": continue
        data_dict[key] = block.find(class_=field).get_text()

        # make sure all timestamps are in the same format for easy SQL parsing
        if key == "time":
            class_time = datetime.datetime.strptime(data_dict['time'], data['timeformat'])
            class_time = class_time.replace(year=datetime.datetime.now().year)
            data_dict['time'] = class_time.strftime("%Y-%m-%d %H:%M")

    return data_dict


def load_config(filename):
    '''
    load a json from filename and return the dictionary
    '''
    with open(os.path.join(BASE_FILEPATH, filename)) as config_file:
        return json.loads(config_file.read())


def handle_closed(urls, data):
    '''
    Iterate over the list of closed class urls, and if they show up in our
    previous records, call the close method of the SpinClass to update
    the seat count. Currently ignores closed classes that we have never
    seen open.
    '''
    for url in urls:
        if url in data:
            data[url].set_sold_out()
        else:
            data[url] = SpinClass(url, None, None, [], [], None)
    return data


def browse(browser, url):
    '''
    browse visit a website and re-log in if the site has booted us
    '''
    # if we are already at the desired page, we're good!
    if browser.url == url: return
    browser.visit(url)
    # allow time for the page to load
    time.sleep(CONFIG["browse_sleep"])
    # sometimes the site will boot us and require a new login. Check
    # to see if that has happened and login if necessary
    if 'login' in SITE_CONFIG[company] and SITE_CONFIG[company]['login']['login_url'] in browser.url:
        log_in(browser)
        time.sleep(CONFIG["browse_sleep"])


def log_in(browser):
    '''
    If a website requries a login to view seats, fill the required login form
    and submit it using a random name/password combo stored in the config file.
    '''
    config = SITE_CONFIG[company]['login']
    username_index = random.randrange(len(config['names']))
    username = config['names'][username_index]
    password = config['passwords'][username_index]
    browser.find_by_name(config['username_input_name']).first.fill(username)
    browser.find_by_name(config['password_input_name']).first.fill(password)
    browser.find_by_text(config['submit_text']).last.click()


def get_latest_file(company):
    '''
    returns the filepath for the last csv file for the company
    '''
    csv_filepath = os.path.join(BASE_FILEPATH, "csvs")
    latest_csvs = os.listdir(csv_filepath)
    for data_file in latest_csvs:
        if company in data_file:
            return data_file

    return None


def scrape_sites():
    '''
    main function to iterate over all the company sites in the config file
    and create csv files containing their class information.
    '''
    global SITE_CONFIG, company, CONFIG
    SITE_CONFIG = load_config("config/websites.json")
    CONFIG = load_config("config/configuration.json")

    csv_filepath = os.path.join(BASE_FILEPATH, "csvs")
    backup_csvs_filepath = os.path.join(csv_filepath, "backups")

    # keep track of how long the scrape takes in a log
    time_started = datetime.datetime.now()
    with Browser() as browser:
        for company, site_hierarchy in SITE_CONFIG.items():

            # handle backing up and loading old data
            latest_file = get_latest_file(company)
            if latest_file:
                data_dictionary = load_from_csv(os.path.join(csv_filepath, latest_file))

            else:
                data_dictionary = {}

            url_list = {"open": [site_hierarchy['url']]}
            for state in site_hierarchy['order'][:-1]:
                print("URL List {}".format(url_list))
                url_list = return_urls(browser, copy.deepcopy(url_list['open']), site_hierarchy, state)
                if url_list.get('closed'):
                    data_dictionary = handle_closed(url_list['closed'], data_dictionary)

            # now that we have the pages with the seats, count open bikes on them
            state = site_hierarchy['order'][-1]
            print("URLS", url_list['open'])
            for url in url_list['open']:
                seats = {}
                for tag, filters in site_hierarchy[state]['tags'].items():
                    seats[tag] = []
                    seat_blocks = select_blocks(browser, url, filters)
                    for seat in seat_blocks:
                        seats[tag].append(seat.get_text())

                # some sites have placeholders or do not label an open/closed type explicitly
                # use the subtraction tag to determine the true amounts, assuming there is a
                # total and either an open/closed field
                if site_hierarchy[state].get('subtract'):
                    open_seats = []
                    closed_seats = []

                    total_spaces = seats['total']
                    if 'ignore' in seats:
                        total_spaces = [space for space in total_spaces if space not in seats['ignore']]

                    if 'open' in seats:
                        closed_seats = [space for space in total_spaces if space not in seats['open']]
                        open_seats = seats['open']

                    if 'closed'in seats:
                        open_seats = [seat for space in total_spaces if space not in seats['closed']]
                        closed_seats = seats['closed']

                    seats = {}
                    seats['open'] = open_seats
                    seats['closed'] = closed_seats

                data_block = find_block(browser, url, site_hierarchy[state]['data']['header'])
                if data_block is None:
                    banner_blocks = select_blocks(browser, url, site_hierarchy[state]['exceptions']['header'])
                    if banner_blocks is None:
                        print("Could not find the {} at {}".format(site_hierarchy[state]['data']['header'], url))
                    else:
                        for banner_block in banner_blocks:
                            banner_text = banner_block.get_text()
                            if site_hierarchy[state]['exceptions']['closed'] in banner_text:
                                if url in data_dictionary:
                                    data_dictionary[url].set_sold_out()
                                else:
                                    data_dictionary[url] = SpinClass(url, None, None, [], [], None)
                else:
                    data = grab_data(data_block, site_hierarchy[state]['data'])
                    data_dictionary[url] = SpinClass(url, data['time'], data['instructor'], seats['closed'], seats['open'], data['location'])
            write_to_csv(data_dictionary, company, 'csvs/{}-{}.csv'.format(company, datetime.datetime.now().strftime("%Y-%m-%dT%H-%M")), 'w')
            write_to_csv(data_dictionary, company, 'csvs/backups/{}-master.csv'.format(company), 'a')

            if latest_file: os.system('mv {} {}'.format(os.path.join(csv_filepath, latest_file), backup_csvs_filepath))

            # update the log to keep track of how long it took to run
            time_finished = datetime.datetime.now()
            elapsed_time = (time_finished-time_started).total_seconds()
            with open('time_log.log', 'a') as time_log:
                time_log.write('{}: {}\n'.format(time_started.strftime("%Y-%m-%dT%H-%M"), elapsed_time))


def write_to_csv(data, company, filepath, writetype='w'):
    '''
    take dictionary of url: SoulClass objects and save a csv file that can be restored later.
    '''

    scrape_time = datetime.datetime.now().strftime("%Y-%m-%dT%H-%M")
    dict_list = []
    for class_url, data in data.items():
        # add the timestamp to the data saved to csv
        full_dict = data.get_dictionary()
        full_dict['scrape_time'] = scrape_time
        dict_list.append(full_dict)
    keys = dict_list[0].keys()
    with open(os.path.join(BASE_FILEPATH, filepath), writetype) as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(dict_list)


def load_from_csv(csv_filename):
    '''
    read in a csv file from csv_filename and save a dictionary of url:SpinClass pairs
    '''
    newdict = {}
    try:
        with open(csv_filename, 'r') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                # don't repeatedly report empty classes
                if not row['time']: continue
                try:
                    # check to see if the date has passed. If not, include it in
                    # this data as well. TODO: take year and timezone in to
                    # account
                    class_time = datetime.datetime.strptime(row['time'], "%Y-%m-%d %H:%M")
                    if class_time > datetime.datetime.now():
                        newdict[row['url']] = SpinClass(row['url'],
                                                        row['time'],
                                                        row['instructor'],
                                                        ast.literal_eval(row['seats_taken']),
                                                        ast.literal_eval(row['seats_open']),
                                                        row['location'])
                    else:
                        print("Class at {} is already over".format(row['time']))

                except Exception as e:
                        print(e)
                        print("Could not get SpinClass data from {}".format(row))
                        continue
    except Exception as e:
        print(e)
        print("Could not find previous file at {}".format(csv_filename))
    return newdict

if __name__ == '__main__':
        parser = argparse.ArgumentParser()
        parser.add_argument('-t' , '--testing', action='store_true', help='Do not use the singleton function as it is currently not working in python3. This is for command line testing')
        args = parser.parse_args()

        if not args.testing:
            from quicklock import singleton
            # make sure only one instance is running
            singleton('ClassScraper')

        # begin scraping
        scrape_sites()
