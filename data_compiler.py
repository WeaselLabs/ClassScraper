#!/usr/local/bin/python
import csv
from pysqldf import SQLDF
import os
import datetime
import time
import pandas

BASE_FILEPATH = os.path.realpath(os.path.dirname(__file__))
backups_directory = os.path.join(BASE_FILEPATH, 'csvs/backups')
compiled_directory = os.path.join(BASE_FILEPATH, 'csvs/compiled')
if not os.path.isdir(compiled_directory):
	os.mkdir(compiled_directory)

sqldf = SQLDF(globals())


for filename in os.listdir(backups_directory):
	if 'master' in filename:
		master = pandas.read_csv(os.path.join(backups_directory, filename))
		company = filename.split('-')[0]


		datatoprint = sqldf.execute("SELECT * FROM master as u INNER JOIN( SELECT url, max(scrape_time) as time_stamp FROM master GROUP BY 1) as q ON u.url = q.url AND u.scrape_time = q.time_stamp;")
		datatoprint.to_csv(os.path.join(compiled_directory, '{}-compiled-{}.csv'.format(company, time.strftime("%Y%m%d-%H%M%S"))))
