class SpinClass:

    def __init__(self, url, time, instructor,
                 seats_taken, seats_open, location):
        self.time = time
        self.instructor = instructor
        self.seats_taken = seats_taken
        self.seats_open = seats_open
        self.seats_total = self.get_seats_total()
        self.url = url
        self.location = location

    def get_seats_total(self):
        return len(self.seats_open) + len(self.seats_taken)

    def set_sold_out(self):
        self.seats_taken += self.seats_open
        self.seats_open = []

    def get_dictionary(self):
        return {'time':self.time,
                'instructor':self.instructor,
                'seats_taken':self.seats_taken,
                'seats_open':self.seats_open,'url':self.url,
                'seat_open_count':len(self.seats_open),
                'seat_taken_count':len(self.seats_taken),
                'total_seats':self.get_seats_total(),
                'location':self.location}
